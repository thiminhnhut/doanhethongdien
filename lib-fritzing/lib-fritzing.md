# Mã nguồn của thư viện Fritzing

1. [Demo Sketch ESP8266.fzz](https://gitlab.com/thiminhnhut/doanhethongdien/blob/master/lib-fritzing/Demo%20Sketch%20ESP8266.fzz).
		
	Tác giả [mcauser](https://github.com/mcauser) trên GitHub: https://github.com/mcauser/Fritzing-Part-WeMos-D1-Mini
						
2. [OLED_096_128x64_I2C_SSD1306.fzpz](https://gitlab.com/thiminhnhut/doanhethongdien/blob/master/lib-fritzing/OLED_096_128x64_I2C_SSD1306.fzpz).
		
	Tác giả [squix78](https://github.com/squix78) trên Github: https://github.com/squix78/esp8266-fritzing-parts/tree/master/OLED_SSD1306_I2C_128x64
		
3. [Adafruit BME280.fzpz](https://gitlab.com/thiminhnhut/doanhethongdien/blob/master/lib-fritzing/Adafruit%20BME280.fzpz).
				
	Tác giả [adafruit](https://github.com/adafruit) trên GitHub: https://github.com/adafruit/Fritzing-Library/blob/master/parts/Adafruit%20BME280.fzpz
