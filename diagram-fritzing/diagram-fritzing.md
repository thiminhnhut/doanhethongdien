# Sơ đồ mạch vẽ bằng phần mềm Fritzing

1. Kết nối module ESP8266 Wemos D1 Mini và màn hình OLED với giao tiếp I2C:
	* File Fritzing: [esp8266-wemos-d1-mini-and-oled.fzz](https://gitlab.com/thiminhnhut/doanhethongdien/blob/master/diagram-fritzing/esp8266-wemos-d1-mini-and-oled.fzz)	
	* File ảnh: [esp8266-wemos-d1-mini-and-oled.png](https://gitlab.com/thiminhnhut/doanhethongdien/raw/master/diagram-fritzing/esp8266-wemos-d1-mini-and-oled.png)
2. Kết nối module ESP8266 Wemos D1 Mini và cảm biến BME280 với giao tiếp I2C:
	* Địa chỉ I2C là 0x76:	
		+ File Fritzing: [esp8266-wemos-d1-mini-and-bme280-i2c-0x76.fzz](https://gitlab.com/thiminhnhut/doanhethongdien/blob/master/diagram-fritzing/esp8266-wemos-d1-mini-and-bme280-i2c-0x76.fzz)	
		+ File ảnh: [esp8266-wemos-d1-mini-and-bme280-i2c-0x76.png](https://gitlab.com/thiminhnhut/doanhethongdien/raw/master/diagram-fritzing/esp8266-wemos-d1-mini-and-bme280-i2c-0x76.png)	
	* Địa chỉ I2C là 0x77:		
		+ File Fritzing: [esp8266-wemos-d1-mini-and-bme280-i2c-0x77.fzz](https://gitlab.com/thiminhnhut/doanhethongdien/blob/master/diagram-fritzing/esp8266-wemos-d1-mini-and-bme280-i2c-0x77.fzz)	
		+ File ảnh: [esp8266-wemos-d1-mini-and-bme280-i2c-0x77.png](https://gitlab.com/thiminhnhut/doanhethongdien/raw/master/diagram-fritzing/esp8266-wemos-d1-mini-and-bme280-i2c-0x77.png)
3. Kết nối module ESP8266 Wemos D1 Mini và cảm biến BME280 với giao tiếp SPI:
	* File Fritzing: [esp8266-wemos-d1-mini-and-bme280-spi.fzz](https://gitlab.com/thiminhnhut/doanhethongdien/blob/master/diagram-fritzing/esp8266-wemos-d1-mini-and-bme280-spi.fzz)	
	* File ảnh: [esp8266-wemos-d1-mini-and-bme280-spi.png](https://gitlab.com/thiminhnhut/doanhethongdien/raw/master/diagram-fritzing/esp8266-wemos-d1-mini-and-bme280-spi.png)
4. Kết nối module ESP8266 Wemos D1 Mini với cảm biến BME280 và màn hình OLED dùng giao tiếp I2C:
	* Địa chỉ I2C là 0x76:	
		+ File Fritzing: [esp8266-wemos-d1-mini-and-oled-and-bme280-i2c-0x76.fzz](https://gitlab.com/thiminhnhut/doanhethongdien/blob/master/diagram-fritzing/esp8266-wemos-d1-mini-and-oled-and-bme280-i2c-0x76.fzz)	
		+ File ảnh: [esp8266-wemos-d1-mini-and-oled-and-bme280-i2c-0x76.png](https://gitlab.com/thiminhnhut/doanhethongdien/raw/master/diagram-fritzing/esp8266-wemos-d1-mini-and-oled-and-bme280-i2c-0x76.png)		
	* Địa chỉ I2C là 0x77:	
		+ File Fritzing: [esp8266-wemos-d1-mini-and-oled-and-bme280-i2c-0x77.fzz](https://gitlab.com/thiminhnhut/doanhethongdien/blob/master/diagram-fritzing/esp8266-wemos-d1-mini-and-oled-and-bme280-i2c-0x77.fzz)	
		+ File ảnh: [esp8266-wemos-d1-mini-and-oled-and-bme280-i2c-0x77.png](https://gitlab.com/thiminhnhut/doanhethongdien/raw/master/diagram-fritzing/esp8266-wemos-d1-mini-and-oled-and-bme280-i2c-0x77.png)
5. Kết nối module ESP8266 Wemos D1 Mini với cảm biến BME280 và màn hình OLED dùng giao tiếp SPI:
	* File Fritzing: [esp8266-wemos-d1-mini-and-oled-and-bme280-spi.fzz](https://gitlab.com/thiminhnhut/doanhethongdien/blob/master/diagram-fritzing/esp8266-wemos-d1-mini-and-oled-and-bme280-spi.fzz)	
	* File ảnh: [esp8266-wemos-d1-mini-and-oled-and-bme280-spi.png](https://gitlab.com/thiminhnhut/doanhethongdien/raw/master/diagram-fritzing/esp8266-wemos-d1-mini-and-oled-and-bme280-spi.png)
