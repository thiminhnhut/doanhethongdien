# Mã nguồn của thư viện Arduino

1. [Adafruit_GFX.zip](https://github.com/adafruit/Adafruit-GFX-Library)

	Tác giả [adafruit](https://github.com/adafruit) trên Github: https://github.com/adafruit/Adafruit-GFX-Library

2. [Adafruit_SSD1306.zip](https://github.com/mcauser/Adafruit_SSD1306/tree/esp8266-64x48)

	Tác giả [mcauser](https://github.com/mcauser) trên Github: https://github.com/mcauser/Adafruit_SSD1306/tree/esp8266-64x48
	
3. [cactus_io_BME280_I2C.zip](http://static.cactus.io/downloads/library/bme280/cactus_io_BME280_I2C.zip)

	Tác giả [cactus.io](http://cactus.io/): http://cactus.io/hookups/sensors/barometric/bme280/hookup-arduino-to-bme280-barometric-pressure-sensor
	
4. [cactus_io_BME280_SPI.zip](http://static.cactus.io/downloads/library/bme280/cactus_io_BME280_I2C.zip)

	Tác giả [cactus.io](http://cactus.io/): http://cactus.io/hookups/sensors/barometric/bme280/hookup-arduino-to-bme280-barometric-pressure-sensor-spi

5. [ThingSpeak.zip](https://github.com/iobridge/thingspeak)

	Tác giả [iobridge](https://github.com/iobridge) trên Github: https://github.com/iobridge/thingspeak

6. [Blynk](https://github.com/blynkkk/blynk-library)
	
	Tác giả [blynkkk](https://github.com/blynkkk) trên Github: https://github.com/blynkkk/blynk-library
	
7. [SimpleTimer.zip](https://github.com/jfturcot/SimpleTimer):

	Tác giả [jfturcot](https://github.com/jfturcot) trên Github: https://github.com/jfturcot/SimpleTimer
